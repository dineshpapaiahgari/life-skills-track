# Understanding Browser Rendering: From HTML to DOM

When you type a URL into your browser and hit enter, a complex series of events unfold behind the scenes to transform code into the visually rich web pages we interact with daily. The process involves fetching the HTML, CSS, and JavaScript, and rendering them into a Document Object Model (DOM). Here, we will delve into the details of how browsers accomplish this.

## 1. Fetching Resources

The first step in the browser rendering process is fetching the web resources. When you request a web page, the browser sends an HTTP request to the server. The server responds with the HTML document, and the browser starts parsing it immediately. During this parsing, the browser may encounter additional resources such as CSS files, JavaScript files, images, and other assets, which it requests in parallel.

 ## 2. HTML Parsing and DOM Construction
As the browser parses the HTML, it constructs the DOM (Document Object Model). The DOM is a tree-like structure where each HTML element becomes a node. This process is known as parsing. During parsing, the browser encounters different elements like \<div\>, \<p\>, \<img\>, etc., and creates corresponding nodes in the DOM tree.

Here's a simplified example:
```
<!DOCTYPE html>
<html>
  <head>
    <title>Sample Page<title>
  </head>
  <body>
    <h1>Hello, World!</h1>
    <p>This is a sample page.</p>
  </body>
</html> 
```
The DOM tree for this HTML might look like this:
```
Document
  └── html
      ├── head
      │   └── title
      │       └── "Sample Page"
      └── body
         ├── h1
         │   └── "Hello, World!"
         └── p
             └── "This is a sample page."
```

 ## 3. CSS Parsing and the CSSOM
Simultaneously, the browser fetches and parses CSS files. This process creates another tree structure called the CSSOM (CSS Object Model). The CSSOM represents the styles applied to various elements in the DOM. The CSSOM is crucial for determining how elements should be styled and displayed on the screen.

For example, a CSS rule like:
```
<style>
h1 {
  color: blue;
}
</style>
```

would result in a CSSOM rule that associates the h1 elements in the DOM with the color blue.

## 4. JavaScript Execution
JavaScript can modify both the DOM and the CSSOM. When the browser encounters a `<script>` tag, it must pause the HTML parsing to fetch, parse, and execute the JavaScript code. This execution can potentially alter the DOM and CSSOM by adding, removing, or changing elements and styles. This is why scripts can block the rendering process, making asynchronous or deferred loading of scripts a common practice to improve performance.

## 5. Constructing the Render Tree
Once the DOM and CSSOM are fully constructed, the browser combines them to create the Render Tree. The Render Tree contains only the visible content and the computed styles. It excludes non-visible elements (like those hidden with display: none).

## 6. Layout
With the Render Tree ready, the browser starts the layout process, where it calculates the exact position and size of each element. This step is also known as "reflow." During layout, the browser takes into account the dimensions and relationships between elements as defined by the CSS.

## 7. Painting
After the layout is complete, the browser proceeds to the painting phase. This is where it fills in pixels on the screen. The painting process involves drawing text, colors, borders, shadows, and other visual aspects of elements.

## 8. Compositing
Finally, modern browsers use a process called compositing to handle complex pages efficiently. During compositing, the browser divides the page into several layers, which are painted separately and then combined into the final image you see. This technique enhances performance, particularly for animations and transformations, by allowing the browser to repaint only the layers that change.

# Conclusion
Browser rendering is a sophisticated process involving multiple steps: fetching resources, parsing HTML and CSS, executing JavaScript, constructing the DOM and CSSOM, creating the Render Tree, layout, painting, and compositing. Each step is critical for transforming raw code into the interactive web pages we use every day. Understanding this process can help web developers create more efficient and performant websites.

# References
1. [What is Browser Rendering?](https://www.quora.com/What-is-Browser-Rendering)   
2. [How browser rendering works — behind the scenes](https://blog.logrocket.com/how-browser-rendering-works-behind-scenes/)    
3. [Populating the page: how browsers work]( https://developer.mozilla.org/en-US/docs/Web/Performance/How_browsers_work)     
